class CreateCosts < ActiveRecord::Migration
  def change
    create_table :costs do |t|
      t.references :category, index: true, foreign_key: true
      t.integer :amount

      t.timestamps null: false
    end
  end
end
